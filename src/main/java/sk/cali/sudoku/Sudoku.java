package sk.cali.sudoku;

import java.io.*;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Sudoku {

    private Integer[][] matrixRowed = new Integer[9][9];
    private Integer[][][] matrixBoxed = new Integer[3][3][9];
    private Integer[][] matrixColumned = new Integer[9][9];
    private List<Integer[]> matrixIndexes = new ArrayList<>();
    private Path path;

    public Sudoku(Path path) {
        this.path = path;
    }

    public void createFile(List<Integer[][]> matrixes) throws IOException {
        FileWriter writer = null;
        try {
            File myObj = new File("top95.out");
            myObj.createNewFile();
            writer = new FileWriter(myObj);
            String line = "";
            for (int i = 0; i < matrixes.size(); i++) {
                for (int j = 0; j < matrixes.get(i).length; j++) {
                    line += Arrays.asList(matrixes.get(i)[j])
                            .stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining());
                }
                line += "\n";
            }
            writer.write(line);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }

    public List<Integer[][]> getSolvedMatrix() {
        List<Integer[][]> solvedMatrixes = getMatrixFromFile(this.path);
        ;
        for (int i = 0; i < solvedMatrixes.size(); i++) {
            this.matrixRowed = solvedMatrixes.get(i);
            this.matrixBoxed = createMatrixBoxed(matrixRowed);
            this.matrixColumned = createMatrixColumned(matrixRowed);
            this.matrixIndexes = getPositionOfUnfilledNumber(matrixRowed);
            solvedMatrixes.set(i, solveSudoku(this.matrixRowed, 0));
        }

        return solvedMatrixes;
    }

    private List<Integer[]> getPositionOfUnfilledNumber(Integer[][] matrix) {
        List<Integer[]> matrixIndexes = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == 0) matrixIndexes.add(new Integer[]{i, j});
            }
        }
        return matrixIndexes;
    }

    private List<Integer[][]> getMatrixFromFile(Path path) {
        File file = new File(path.toUri());
        List<Integer[][]> matrixes = new ArrayList<>();

        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            int i = 0;
            String line = bufferedReader.readLine();
            if (!line.contains("|")) {
                String matrixesInLine = "";
                do {
                    matrixesInLine += line;
                } while ((line = bufferedReader.readLine()) != null);
                for (int j = 0; j < matrixesInLine.length() / 81; j++) {
                    Integer[][] matrix = new Integer[9][9];
                    for (int k = 0; k < 9; k++) {
                        Integer[] matrixLine = matrixesInLine.substring(j * 81 + (k * 9), j * 81 + (k * 9 + 9)).chars()
                                .mapToObj(c -> String.valueOf((char) c))
                                .map(x -> (x.equals(" ")) ? x.replace(" ", "0") : x)
                                .map(x -> (x.equals(".")) ? x.replace(".", "0") : x)
                                .filter(x -> x.matches("[0-9]"))
                                .map(Integer::valueOf)
                                .toArray(Integer[]::new);
                        if (matrixLine.length > 0) {
                            matrix[i++] = matrixLine;
                        }
                    }
                    i = 0;
                    matrixes.add(matrix);
                }
            } else {
                Integer[][] matrix = new Integer[9][9];
                do {
                    Integer[] matrixLine = line
                            .chars()
                            .mapToObj(c -> String.valueOf((char) c))
                            .map(x -> (x.equals(" ")) ? x.replace(" ", "0") : x)
                            .map(x -> (x.equals(".")) ? x.replace(".", "0") : x)
                            .filter(x -> x.matches("[0-9]"))
                            .map(Integer::valueOf)
                            .toArray(Integer[]::new);
                    if (matrixLine.length > 0) {
                        matrix[i++] = matrixLine;
                    }
                } while ((line = bufferedReader.readLine()) != null);
                matrixes.add(matrix);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matrixes;
    }

    private Integer[][] createMatrixColumned(Integer[][] matrix) {
        int x = 9, y = 9;
        Integer[][] matrixColumned = new Integer[x][y];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrixColumned[i][j] = matrix[j][i];
            }
        }
        return matrixColumned;
    }

    private Integer[][][] createMatrixBoxed(Integer[][] matrix) {
        int x = 3, y = 3, z = 9;
        Integer[][][] matrixBoxed = new Integer[x][y][z];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrixBoxed[i / 3][j / 3][((i % 3) * 3) + j % 3] = matrix[i][j];
            }
        }
        return matrixBoxed;
    }

    private Integer[][] solveSudoku(Integer[][] matrix, int index) {
        if (Arrays.stream(matrix).flatMap(xi -> Arrays.stream(xi)).filter(yi -> yi.equals(0)).count() != 0) {
            int x = matrixIndexes.get(index)[0], y = matrixIndexes.get(index)[1];
            for (int k = 1; k <= 9; k++) {
                if (!inBox(x / 3, y / 3, k) && !inRow(x, k) && !inColumn(y, k)) {
                    updateMatrixes(x, y, k);
                    matrix = solveSudoku(matrix, index + 1);
                    if (Arrays.stream(matrix).flatMap(xi -> Arrays.stream(xi)).filter(yi -> yi.equals(0)).count() == 0)
                        return matrix;
                } else {
                    matrix[x][y] = 0;
                    updateMatrixes(x, y, 0);
                }
                if (matrix[x][y] == 9 && index < matrixIndexes.size() - 1 && matrix[matrixIndexes.get(index + 1)[0]][matrixIndexes.get(index + 1)[1]] == 0) {
                    matrix[x][y] = 0;
                    updateMatrixes(x, y, 0);
                }
            }
        }
        return matrix;
    }

    private void updateMatrixes(int x, int y, int k) {
        matrixBoxed[x / 3][y / 3][((x % 3) * 3) + y % 3] = k;
        matrixRowed[x][y] = k;
        matrixColumned[y][x] = k;
    }

    private boolean inBox(int i, int j, Integer num) {
        if (Arrays.asList(matrixBoxed[i][j]).contains(num)) return true;
        return false;
    }

    private boolean inColumn(int index, Integer num) {
        if (Arrays.asList(matrixColumned[index]).contains(num)) return true;
        return false;
    }

    private boolean inRow(int index, Integer num) {
        if (Arrays.asList(Arrays.asList(matrixRowed).get(index)).contains(num)) return true;
        return false;
    }

    public static void main(String[] args) throws IOException {
        Sudoku sudoku = new Sudoku(Path.of("/home/cali/DEVELOPMENT/DEVELOPMENT/JAVA/sudoku/src/main/java/sk/cali/sudoku/in/top95.txt"));
        List<Integer[][]> matrixes = sudoku.getSolvedMatrix();
        System.out.println(matrixes.size());
//        matrixes.stream()
//                .map(Arrays::asList)
//                .forEach(x -> x.stream().map(Arrays::asList).forEach(System.out::println));
        sudoku.createFile(matrixes);

    }
}


